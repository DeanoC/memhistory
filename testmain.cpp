//
// Created by Dean Calver on 19/01/2018.
//

#include "gtest/gtest.h"
#include "memhistory.h"
#include "NonOverlapping.h"

TEST(MemHistory, Creation) {
    MemHistory& mem = MemHistory_GetController();
}

TEST(MemHistory, alloc) {
    MemHistory& mem = MemHistory_GetController();
    MemHHandle handle = mem.allocate(256);
    EXPECT_EQ(handle.isValid(), true);
}

TEST(MemHistory, BlockMove) {

    MemHistory::Block a(nullptr, 20);
    MemHistory::Block b(malloc(10), 10);
    EXPECT_EQ(a.ptr, nullptr);
    EXPECT_EQ(a.size, 20);
    EXPECT_NE(b.ptr, nullptr);
    EXPECT_EQ(b.size, 10);
    a = std::move(b);
    EXPECT_NE(a.ptr, nullptr);
    EXPECT_EQ(a.size, 10);
}

TEST(MemHistory, ReadPtr) {
    MemHistory& mem = MemHistory_GetController();
    MemHHandle handle = mem.allocate(4);
    EXPECT_EQ(handle.isValid(), true);
    // memory is always set to zero
    ReadPtr<uint32_t> rptr = handle.readAcquire<uint32_t>();

    EXPECT_TRUE(rptr);
    EXPECT_EQ(rptr.get(), 0);

    ReadPtr<uint32_t> rptr2 = std::move(rptr);

    EXPECT_EQ(rptr2.get(), 0);
    EXPECT_TRUE(!rptr);

    {
        auto rptr3 = rptr2.persist();
        EXPECT_EQ(rptr2.get(), 0);
        EXPECT_TRUE(rptr2);
        EXPECT_EQ(rptr3.get(), 0);
        EXPECT_TRUE(rptr3);
    }
    EXPECT_EQ(rptr2.get(), 0);
    EXPECT_TRUE(rptr2);

}

TEST(MemHistory, MutatingPtr) {
    MemHistory& mem = MemHistory_GetController();
    MemHHandle handle = mem.allocate(4);
    EXPECT_EQ(handle.isValid(), true);

    // memory is always set to zero
    ReadPtr<uint32_t> rptr = handle.readAcquire<uint32_t>();

    EXPECT_TRUE(rptr);
    EXPECT_EQ(rptr.get(), 0);

    {
        MutatingPtr<uint32_t> mptr = handle.mutateAcquire<uint32_t>();
        mptr.get() = 4;

        // until the mutating ptr is done, read pointer previously got
        // is the same
        EXPECT_TRUE(rptr);
        EXPECT_EQ(rptr.get(), 0);

        // until the mutating ptr is done, a new read pointer is still
        // the same
        ReadPtr<uint32_t> rptr2 = handle.readAcquire<uint32_t>();
        EXPECT_TRUE(rptr2);
        EXPECT_EQ(rptr2.get(), 0);
    }
    // read pointer previously done is still the same
    EXPECT_TRUE(rptr);
    EXPECT_EQ(rptr.get(), 0);

    // how ever a new read pointer now points to the finished mutated data
    ReadPtr<uint32_t> rptr2 = handle.readAcquire<uint32_t>();
    EXPECT_TRUE(rptr2);
    EXPECT_EQ(rptr2.get(), 4);

    {
        MutatingPtr<uint32_t> mptr = handle.mutateAcquire<uint32_t>();
        mptr.get() = 1;
        EXPECT_DEATH({
                 MutatingPtr<uint32_t> mptr2 = handle.mutateAcquire<uint32_t>();
                     }, "!prevResult");
    }

    {
        MutatingPtr<uint32_t> mptr = handle.mutateAcquire<uint32_t>();
        mptr.get() = 2;
#if defined(_DEBUG)
        EXPECT_EQ(*((uint32_t *) handle.debugPtr), 2);
#endif
    }

    ReadPtr<uint32_t> rptra = handle.readAcquire<uint32_t>();
    EXPECT_EQ(rptra.get(), 2);

#if defined(_DEBUG)
    EXPECT_EQ(*((uint32_t *) handle.debugPtr), 2);
#endif
}
TEST(MemHistory, BlockResize) {

    MemHistory& mem = MemHistory_GetController();
    MemHHandle handle = mem.allocate(sizeof(int)*5);
    EXPECT_EQ(handle.isValid(), true);
    ReadPtr<int> optr = handle.readAcquire<int>();


    {
        MutatingPtr<int> mptr = handle.mutateAcquire<int>();
        int *ip = &mptr.get();
        ip[0] = 1;
        ip[1] = 2;
        ip[2] = 3;
        ip[3] = 4;
        ip[4] = 5;
    }
    {
        ReadPtr<int> rptr = handle.readAcquire<int>();
        int const * ip = &rptr.get();
        EXPECT_EQ(ip[0],1);
        EXPECT_EQ(ip[1],2);
        EXPECT_EQ(ip[2],3);
        EXPECT_EQ(ip[3],4);
        EXPECT_EQ(ip[4],5);
    }
    {
        MutatingPtr<int> mptr = handle.resizeAcquire<int>(sizeof(int)*3);
        int *ip = &mptr.get();
        EXPECT_EQ(ip[0],1);
        EXPECT_EQ(ip[1],2);
        EXPECT_EQ(ip[2],3);
        ip[0] = 2;
        ip[1] = 3;
        ip[2] = 4;
    }
    {
        ReadPtr<int> rptr = handle.readAcquire<int>();
        int const * ip = &rptr.get();
        EXPECT_EQ(ip[0],2);
        EXPECT_EQ(ip[1],3);
        EXPECT_EQ(ip[2],4);
    }
    {
        MutatingPtr<int> mptr = handle.resizeAcquire<int>(sizeof(int)*6);
        int *ip = &mptr.get();
        EXPECT_EQ(ip[0],2);
        EXPECT_EQ(ip[1],3);
        EXPECT_EQ(ip[2],4);
        EXPECT_EQ(ip[3],0);
        EXPECT_EQ(ip[4],0);
        EXPECT_EQ(ip[5],0);
        ip[0] = 3;
        ip[1] = 4;
        ip[2] = 5;
        ip[3] = 6;
        ip[4] = 7;
        ip[5] = 8;
    }
    {
        ReadPtr<int> rptr = handle.readAcquire<int>();
        int const * ip = &rptr.get();
        EXPECT_EQ(ip[0],3);
        EXPECT_EQ(ip[1],4);
        EXPECT_EQ(ip[2],5);
        EXPECT_EQ(ip[3],6);
        EXPECT_EQ(ip[4],7);
        EXPECT_EQ(ip[5],8);
        int const *oip = &optr.get();
        EXPECT_EQ(oip[0],0);
        EXPECT_EQ(oip[1],0);
        EXPECT_EQ(oip[2],0);
        EXPECT_EQ(oip[3],0);
        EXPECT_EQ(oip[4],0);
    }

}

TEST(NonOverlapping, ReadPtr) {
    MemHistory& mem = MemHistory_GetController();
    NonOverlapping<int> no(5);
    NonOverlappingReadPtr<int> rptr = no.readAcquire(0);
    EXPECT_EQ(rptr.get(),0);
}

TEST(NonOverlapping, Mutation) {
    MemHistory& mem = MemHistory_GetController();
    NonOverlapping<int> no(5);
    {
        NonOverlappingReadPtr<int> rptr0 = no.readAcquire(0);
        EXPECT_EQ(rptr0.get(), 0);
        no.beginMutation();
        int *seg0 = no.getSegmentForMutation(0);
        int *seg1 = no.getSegmentForMutation(1);
        *seg0 = 10;
        *seg1 = 20;
        EXPECT_EQ(rptr0.get(), 0);
        no.endMutation();
    }
    NonOverlappingReadPtr<int> rptr0 = no.readAcquire(0);
    NonOverlappingReadPtr<int> rptr1 = no.readAcquire(1);
    EXPECT_EQ(rptr0.get(),10);
    EXPECT_EQ(rptr1.get(),20);

}