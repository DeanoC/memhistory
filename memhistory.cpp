//
// Created by Dean Calver on 19/01/2018.
//

#include "memhistory.h"

std::unique_ptr<MemHistory> theMemHistory;

MemHistory& MemHistory_GetController()
{
    if(!theMemHistory) {
        theMemHistory = std::make_unique<MemHistory>();
    }

    return *theMemHistory.get();
}

MemHHandle MemHistory::allocate(uint64_t size)
{
    auto it = handles.grow_by(1);
    *it = allocateBlock(size);

    return MemHHandle(it - handles.begin());
}

void MemHistory::free(MemHHandle handle)
{
    if(!handle.isValid()) return;

    // currently we don't ever reuse the handle index itself
    releaseBlock(handles[handle.handleIndex]);
}

BlockIndex_t MemHistory::allocateBlock(uint64_t size) {

    auto it = blocks.grow_by(1);
    it->ptr = malloc(size);
    it->size = size;
    it->readCount = 1;

    if(it->ptr == nullptr) {
        return InvalidIndex<BlockIndex_t>();
    }

    // TODO don't do this if OS does it
    memset(it->ptr, 0, size);

    return BlockIndex_t(it - blocks.begin());
}

void MemHistory::releaseBlock(BlockIndex_t index) {
    Block& block = blocks[index];
    block.decrementReadCount();
}

void MemHistory::blockToTheFuture(HandleIndex_t index) {
    BlockIndex_t blockIndex = handles[index];
    Block& block = blocks[blockIndex];

    // atomic exhange
    block.futureIndex = handles[index].exchange(block.futureIndex);
    assert(blockIndex == block.futureIndex);

    releaseBlock(blockIndex);

}
