#pragma once

#include <cassert>
#include <atomic>
#include <type_traits>
#include "tbb/tbb.h"

#define _DEBUG

extern class MemHistory& MemHistory_GetController();

using BlockIndex_t = uint32_t;
using HandleIndex_t = uint32_t;

template<typename T>
inline constexpr T InvalidIndex() {
    return T(~0);
}

template<typename T>
class ReadPtr {
public:
    friend class MemHHandle;
    ~ReadPtr();

    explicit operator bool() const {
        return blockIndex != InvalidIndex<BlockIndex_t>();
    }

    // moving allowed
    ReadPtr(ReadPtr &&rhs) noexcept;

    // copy not allowed, each ReadPtr has only a single user at one
    ReadPtr(ReadPtr const& rhs) = delete;

    // Persist get a read ptr with its own seperate lifetime
    ReadPtr persist() const;

    // get a read only reference to the actual data. always thread safe
    [[nodiscard]] T const& get() const;

protected:
    explicit ReadPtr(BlockIndex_t index_) : blockIndex(index_) {};
    BlockIndex_t blockIndex = InvalidIndex<BlockIndex_t>();
};

template<typename T>
class MutatingPtr {
public:
    friend class MemHHandle;
    template<typename TP> friend class NonOverlapping;
    ~MutatingPtr();

    // moving allowed
    MutatingPtr(MutatingPtr &&rhs) noexcept;

    // copy not allowed, only one MutatingPtr to each block at once
    MutatingPtr(MutatingPtr const& rhs) = delete;

    [[nodiscard]] MutatingPtr& operator=(MutatingPtr&& rhs)
    {
        handleIndex = rhs.handleIndex;
        rhs.handleIndex = InvalidIndex<HandleIndex_t>();
        return *this;
    }

    // get a pointer to the data, can be changed at will. always thread safe
    [[nodiscard]] T& get() const;

    MutatingPtr() :  handleIndex(InvalidIndex<HandleIndex_t>()) {}

    friend void swap(MutatingPtr<T>& a, MutatingPtr<T>& b)
    {
        std::swap(b.handleIndex, a.handleIndex);
    }

private:
    explicit MutatingPtr(HandleIndex_t handleIndex_) : handleIndex(handleIndex_) {};

    HandleIndex_t handleIndex;
};

class MemHHandle {
public:
    friend class MemHistory;

    // checks the validaty of this handle
    bool isValid() const
    {
        return  handleIndex != InvalidIndex<HandleIndex_t>();
    }

    // get a read pointer to the memory block, will not change whilst the
    // ReadPtr is alive, a block can have many ReadPtrs (tho they might not all
    // be reading the same version of the block!)
    template<typename T>
    [[nodiscard]] ReadPtr<T> readAcquire() const;

    // get a mutable pointer to the memory block, the can only be one MutatingPtr
    // per block. Updates will be atomic and only visible once the MutatingPtr's
    // lifetime is over.
    // Copy ensure the mutated ptr starts with the current read pointer version,
    // false and it will be zero'ed
    template<typename T>
    [[nodiscard]] MutatingPtr<T> mutateAcquire(bool copy = true) const;

    template<typename T>
    [[nodiscard]] MutatingPtr<T> resizeAcquire(size_t size, bool copy = true) const;

    // invalid handles are allowed
    MemHHandle() = default;
    // handles can be copied
    MemHHandle(MemHHandle const& rhs) : MemHHandle(rhs.handleIndex) {}

#if defined(_DEBUG)
    mutable void const *debugPtr = nullptr;
#endif

private:
    explicit MemHHandle(HandleIndex_t handleIndex_) : handleIndex(handleIndex_) {};
    HandleIndex_t handleIndex = InvalidIndex<HandleIndex_t>();
};

class MemHistory {
public:
    // a block is chunk of memory with tracked read and mutating pointers
    // changes to a block via mutating ptr are 'atomic' reads never get
    // partially updated data. Its either the previous/current good version or
    // a new one when the mutating ptr has finished.
    struct Block {
        void *ptr = nullptr;
        size_t size = 0;
        std::atomic<int32_t> readCount = 0;
        std::atomic<bool> mutating = false;
        std::atomic<BlockIndex_t> futureIndex = InvalidIndex<BlockIndex_t>();

        Block() = default;

        explicit Block(void* ptr_, uint32_t size_) : ptr(ptr_), size(size_) {}

        Block(Block &&other) noexcept {
            *this = std::move(other);
        }

        Block &operator=(Block &&other) noexcept
        {
            ptr = other.ptr;
            size = other.size;
            readCount = other.readCount.load();
            mutating = other.mutating.load();
            futureIndex = other.futureIndex.load();
            return *this;
        }

        // when readCount == 0 release the underlying memory.
        // owner is expected to keep a readCount until its happy for it to be
        // deallocated.
        void decrementReadCount() {
            --readCount;
            if(readCount <= 0) {
                ::free(ptr);
                ptr = nullptr;
                size = 0;
                // todo add the block itself to a free list for re-use
            }
        }
    };

    MemHHandle allocate(uint64_t size);
    void free(MemHHandle handle);

    MemHistory() = default;

    [[nodiscard]] Block* blockAt(BlockIndex_t index) {
        assert(index != InvalidIndex<BlockIndex_t>());
        return &blocks[index];
    }
    [[nodiscard]] Block const* blockAt(BlockIndex_t index) const {
        assert(index != InvalidIndex<BlockIndex_t>());
        return &blocks[index];
    }

    [[nodiscard]] BlockIndex_t allocateBlock(uint64_t size);
    void releaseBlock(BlockIndex_t index);

    [[nodiscard]] BlockIndex_t blockIndexAt(HandleIndex_t index) const { return handles[index]; }

    void blockToTheFuture(HandleIndex_t index);

    MemHistory& operator=(MemHistory const&) = delete;

private:

    tbb::concurrent_vector<Block> blocks;
    tbb::concurrent_vector<std::atomic<BlockIndex_t>> handles;

//    tbb::concurrent_vector<BlockIndex_t> freeBlocks;
};

template<typename T>
ReadPtr<T> MemHHandle::readAcquire() const {
    assert(handleIndex != InvalidIndex<HandleIndex_t>());

    MemHistory& mh =  MemHistory_GetController();
    auto blockIndex = mh.blockIndexAt(handleIndex);
    assert(blockIndex != InvalidIndex<BlockIndex_t>());
    auto block = mh.blockAt(blockIndex);
    assert(block != nullptr);

    std::atomic_fetch_add(&block->readCount, 1);

#if defined(_DEBUG)
    debugPtr = block->ptr;
#endif
    return ReadPtr<T>(blockIndex);
}

template<typename T>
MutatingPtr<T> MemHHandle::mutateAcquire(bool copy) const {
    assert(handleIndex != InvalidIndex<HandleIndex_t>());

    MemHistory& mh =  MemHistory_GetController();
    auto blockIndex = mh.blockIndexAt(handleIndex);
    assert(blockIndex != InvalidIndex<BlockIndex_t>());
    auto srcBlock = mh.blockAt(blockIndex);

    bool prevResult = srcBlock->mutating.exchange(true);
    assert(!prevResult); // if this fires, 2 thing are mutating the same block at the same time!

    // COW semantics, so allocate a new block
    BlockIndex_t newBlockIndex = mh.allocateBlock(srcBlock->size);

    srcBlock->futureIndex = newBlockIndex;
    if(copy)
    {
        auto newBlock = mh.blockAt(srcBlock->futureIndex);
        memcpy(newBlock->ptr, srcBlock->ptr, srcBlock->size);
    }

#if defined(_DEBUG)
    auto newBlock = mh.blockAt(srcBlock->futureIndex);
    debugPtr = newBlock->ptr;
#endif

    return MutatingPtr<T>(handleIndex);
}

template<typename T>
MutatingPtr<T> MemHHandle::resizeAcquire(size_t size, bool copy) const {
    assert(handleIndex != InvalidIndex<HandleIndex_t>());
    assert((size/sizeof(T))*sizeof(T) == size);

    MemHistory& mh =  MemHistory_GetController();
    auto blockIndex = mh.blockIndexAt(handleIndex);
    assert(blockIndex != InvalidIndex<BlockIndex_t>());
    auto srcBlock = mh.blockAt(blockIndex);

    bool prevResult = srcBlock->mutating.exchange(true);
    assert(!prevResult); // if this fires, 2 thing are mutating the same block at the same time!

    // COW semantics, so allocate a new block
    BlockIndex_t newBlockIndex = mh.allocateBlock(size);

    srcBlock->futureIndex = newBlockIndex;
    if(copy)
    {
        size_t smallSize = std::min(size, srcBlock->size);
        auto newBlock = mh.blockAt(srcBlock->futureIndex);
        memcpy(newBlock->ptr, srcBlock->ptr, smallSize);
    }

#if defined(_DEBUG)
    auto newBlock = mh.blockAt(srcBlock->futureIndex);
    debugPtr = newBlock->ptr;
#endif

    return MutatingPtr<T>(handleIndex);
}

template<typename T>
ReadPtr<T>::~ReadPtr()
{
    if(blockIndex != InvalidIndex<BlockIndex_t>())
    {
        MemHistory& mh =  MemHistory_GetController();
        auto block = mh.blockAt(blockIndex);
        block->decrementReadCount();
    }
}

template<typename T>
MutatingPtr<T>::~MutatingPtr()
{
    if(handleIndex != InvalidIndex<HandleIndex_t>())
    {
        MemHistory &mh = MemHistory_GetController();
        mh.blockToTheFuture(handleIndex);
    }
}

template<typename T>
ReadPtr<T>::ReadPtr(ReadPtr<T> &&rhs) noexcept
{
    std::swap(blockIndex, rhs.blockIndex);
}

template<typename T>
MutatingPtr<T>::MutatingPtr(MutatingPtr &&rhs) noexcept : handleIndex(InvalidIndex<HandleIndex_t>())
{
    std::swap(handleIndex, rhs.handleIndex);
}

template<typename T>
ReadPtr<T> ReadPtr<T>::persist() const
{
    MemHistory& mh = MemHistory_GetController();
    auto block = mh.blockAt(blockIndex);
    block->readCount += 1;
    return ReadPtr(blockIndex);
}

template<typename T>
T const& ReadPtr<T>::get() const
{
    MemHistory& mh =  MemHistory_GetController();
    auto const block = mh.blockAt(blockIndex);
    void* ptr = block->ptr;
    return *static_cast<T const*>(ptr);
}

template<typename T>
T & MutatingPtr<T>::get() const
{
    MemHistory& mh =  MemHistory_GetController();
    auto const srcBlockIndex = mh.blockIndexAt(handleIndex);
    auto const srcBlock = mh.blockAt(srcBlockIndex);
    auto const futureBlock = mh.blockAt(srcBlock->futureIndex);
    void* ptr = futureBlock->ptr;
    return *static_cast<T*>(ptr);
}


