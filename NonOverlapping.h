#pragma once

#include "memhistory.h"

using SegmentIndex_t = uint32_t;

template<typename T>
class NonOverlappingReadPtr : public ReadPtr<T>{
public:
    template<typename TP> friend class NonOverlapping;

    // get a read only reference to the actual data. always thread safe
    T const& get() const;

private:
    explicit NonOverlappingReadPtr(ReadPtr<T> && rptr, SegmentIndex_t segment_)
            : ReadPtr<T>(std::move(rptr))
            , segment(segment_) {};

    SegmentIndex_t segment = InvalidIndex<SegmentIndex_t>();
};

template<typename T>
class NonOverlapping {
public:
    template<typename TP> friend class NonOverlappingReadPtr;

    NonOverlapping(size_t numElements_);

    void beginMutation(bool copy = true);
    void resizeMutation(size_t numElements_, bool copy = true);
    T* getSegmentForMutation(uint32_t segment);
    void endMutation();

    // begin and ends a mutation cycle resizing as it does
    void resize(size_t numElements_);

    NonOverlappingReadPtr<T> readAcquire(uint32_t segment);

private:
    size_t numElements;
    MutatingPtr<T> mutation;
    MemHHandle blockHandle;
};

template<typename T>
inline void NonOverlapping<T>::beginMutation(bool copy) {
    mutation = blockHandle.mutateAcquire<T>(copy);
}

template<typename T>
inline void NonOverlapping<T>::resizeMutation(size_t numElements_,bool copy){
    mutation = blockHandle.resizeAcquire<T>(numElements_, copy);
}

template<typename T>
inline void NonOverlapping<T>::endMutation() {
    MutatingPtr<T> mptr;
    std::swap(mutation, mptr);
}

template<typename T>
inline void NonOverlapping<T>::resize(size_t numElements_){
    MutatingPtr<T> mptr = blockHandle.resizeAcquire<T>(numElements_, true);
}

template<typename T>
inline T* NonOverlapping<T>::getSegmentForMutation(uint32_t segment)
{
    T* base = &mutation.get();
    return base + segment;
}


template<typename T>
inline NonOverlappingReadPtr<T> NonOverlapping<T>::readAcquire(uint32_t segment)
{
    return NonOverlappingReadPtr<T>(blockHandle.readAcquire<T>(),segment);
}

template<typename T>
inline T const& NonOverlappingReadPtr<T>::get() const
{
    MemHistory& mh =  MemHistory_GetController();
    T const* ptr = &ReadPtr<T>::get();
    return *(ptr+segment);
}
template<typename T>
inline NonOverlapping<T>::NonOverlapping(size_t numElements_)
    : numElements(numElements_)
    , mutation(InvalidIndex<BlockIndex_t>())
{
    MemHistory& mh =  MemHistory_GetController();
    blockHandle = mh.allocate(numElements * sizeof(T));
}
